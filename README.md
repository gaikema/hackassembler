# Assembler
Hack assembler.
This is for Nand2Tetris project 5, in CSCE 312.

---

## Instructions
On Linux, compile using [monodevelop](http://www.monodevelop.com/) or whatever C# compilers are available and run with
``` 
mono Assembler.exe <file.asm>
```
and the Assembler will compile a corresponding `.out.hack` file.
I use the `.out.hack` extension for the binary instruction files in order to not get them confused with the actual `.hack` files,
which I compare them to.

---

## Feedback
Report any issues or bugs [here](https://bitbucket.org/gaikema/hackassembler/issues?status=new&status=open),
or, if you are a grader, just take off points (but please don't actually).

---

## Resources
* [Slides](https://www.dropbox.com/s/ias0qvgtwa20tzt/Topic_07_Assembler.pdf?dl=0)