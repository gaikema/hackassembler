﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft.Json;

namespace Assembler
{
	/// <summary>
	/// Translates Hack assembly language mnemonics into binary codes.
	/// The tables are on page 109.
	/// </summary>
	public class Code
	{
		/// <summary>
		/// Hardcoded dictionary with all the binary jump translations.
		/// </summary>
		private Dictionary<string, string> _jumpDict = new Dictionary<string, string>() 
		{
			// idk about this first one.
			{"", "000"},
			{"JGT", "001"},
			{"JEQ", "010"},
			{"JGE", "011"},
			{"JLT", "100"},
			{"JNE", "101"},
			{"JLE", "110"},
			{"JMP", "111"}
		};

		/// <summary>
		/// Dictionary with dest codes.
		/// </summary>
		private Dictionary<string, string> _destDict = new Dictionary<string, string>()
		{
			{"", "000"},
			{"M", "001"},
			{"D", "010"},
			{"MD", "011"},
			{"A", "100"},
			{"AM", "101"},
			{"AD", "110"},
			{"AMD", "111"}
		};

		/// <summary>
		/// Dictionary with comp codes.
		/// </summary>
		private Dictionary<string, string> _compDict;

		/// <summary>
		/// Initializes a new instance of the <see cref="Assembler.Code"/> class.
		/// </summary>
		public Code()
		{
			// http://stackoverflow.com/a/11882118/5415895
			string sln_dir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
			string str = File.ReadAllText(sln_dir + "/Assembler/Data/comp.json");
			_compDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(str);
		}

		/// <summary>
		/// Return the binary code of the jump mnemonic.
		/// </summary>
		/// <param name="str">String.</param>
		public string Jump(string str)
		{
			return _jumpDict[str];
		}

		/// <summary>
		/// Return the binary code of the dest mnemonic.
		/// </summary>
		/// <param name="str">String.</param>
		public string Dest(string str)
		{
			return _destDict[str];
		}

		/// <summary>
		/// Return the binary code of the compt mnemonic.
		/// </summary>
		/// <param name="str">String.</param>
		public string Comp(string str)
		{
			return _compDict[str];
		}
	}
}

