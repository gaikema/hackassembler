﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Text;
using System.ComponentModel.Design;

namespace Assembler
{
	/// <summary>
	/// Parser.
	/// According to page 113 in the book, the parser "reads the assembly language command,
	/// parsed it, and provides convenient access to the command's components (fields and symbols)."
	/// The book also says it removes all whitespace and comments, but that is done in the lexer.
	/// </summary>
	public class Parser
	{
		/// <summary>
		/// The queue of commands.
		/// </summary>
		private Queue<string> _commands;
		/// <summary>
		/// The code object.
		/// </summary>
		private Code _code;
		/// <summary>
		/// The symbol table.
		/// </summary>
		private SymbolTable _table;
		/// <summary>
		/// The ram.
		/// </summary>
		private int _ram;

		/// <summary>
		/// Initializes a new instance of the <see cref="Assembler.Parser"/> class.
		/// </summary>
		/// <param name="fileName">File name.</param>
		public Parser(string fileName)
		{
			Lexer luthor = new Lexer(fileName);
			_commands = luthor.GetCommands();
			_code = new Code();
			_table = new SymbolTable();
		}

		/// <summary>
		/// Parse the input text.
		/// This function is long and messy.
		/// </summary>
		public string Parse()
		{
			FirstPass();
			// The first 15 spots are taken.
			_ram = 16;
			StringBuilder program = new StringBuilder();

			while(HasMoreCommands())
			{
				//string curr = commands.Peek();
				string currentLine = null;
				// http://stackoverflow.com/questions/3652408/c-sharp-switch-statement-with-without-curly-brackets-whats-the-difference
				switch (CurrentCommandType())
				{
					case CommandType.A_COMMAND:
					{
						string symbol = Symbol();
						currentLine += '0';

						// If the current A-command references a number.
						int ignoreMe;
						if (int.TryParse(symbol, out ignoreMe))
						{
							currentLine += Convert.ToString(Int32.Parse(symbol), 2).PadLeft(15, '0');
						}
						// If the current A-command references a symbol.
						else
						{
							/*
							 * Look up Xxx in the symbol table.
							 * If the symbol is found in the table, 
							 * replace it with its numeric meaning,
							 * and complete the command's translation.
							 * 
							 * If the symbol is not found in the table,
							 * then it must represent a new variable.
							 * Add the pair (Xxx, n) to the symbol table,
							 * where n is the next available RAM address.
							 */

							if (_table.Contains(symbol))
							{
								int address = _table.GetAddress(symbol);
								currentLine += Convert.ToString(address, 2).PadLeft(15, '0');
							}
							else
							{
								_table.AddEntry(symbol, _ram);
								currentLine += Convert.ToString(_ram, 2).PadLeft(15, '0');
								_ram++;
							}
						}
						program.AppendLine(currentLine);
						break;
					}
					case CommandType.C_COMMAND:
					{
						currentLine += "111";
						currentLine += _code.Comp(Comp());
						currentLine += _code.Dest(Dest());
						currentLine += _code.Jump(Jump());
						program.AppendLine(currentLine);
						break;	
					}
					case CommandType.L_COMMAND:
					{
						// Don't do stuff here.
						break;	
					}
					default:
							break;
				}

				Advance();
			}

			return program.ToString();
		}

		/// <summary>
		/// Build the symbol table.
		/// </summary>
		private void FirstPass()
		{
			int rom = 0;
			foreach (string command in _commands)
			{
				switch (GetCommandType(command))
				{
					case CommandType.A_COMMAND:
					{
						rom++;
						break;
					}
					case CommandType.C_COMMAND:
					{
						rom++;
						break;
					}
					case CommandType.L_COMMAND:
					{
						int len = command.Length;
						string symbol = command.Substring(1, len - 2);
						_table.AddEntry(symbol, rom);
						break;
					}
				}
			}
		}

		/// <summary>
		/// Determines whether this instance has more commands.
		/// </summary>
		/// <returns><c>true</c> if this instance has more commands; otherwise, <c>false</c>.</returns>
		private bool HasMoreCommands()
		{
			return (_commands.Count != 0);
		}

		/// <summary>
		/// Reads the next command from the input and makes it the current command.
		/// </summary>
		private void Advance()
		{
			_commands.Dequeue();
		}

		/// <summary>
		/// Returns the type of the current command.
		/// </summary>
		/// <returns>The command type.</returns>
		private CommandType CurrentCommandType()
		{
			return GetCommandType(_commands.Peek());
		}

		/// <summary>
		/// Gets the type of the command.
		/// </summary>
		/// <returns>The command type.</returns>
		/// <param name="command">Command.</param>
		private CommandType GetCommandType(string command)
		{
			// Match a symbol in parenthesis.
			// http://regexr.com/3eetn
			//Regex regi = new Regex("\\(\\w+\\)");
			Regex regi = new Regex("(\\([a-zA-Z][\\w$_.:]+\\))");

			if (command[0] == '@')
			{
				return CommandType.A_COMMAND;
			}
			else if (regi.IsMatch(command))
			{
				return CommandType.L_COMMAND;
			}
			// Just assume that if the command type isn't one of the previous ones, it is a C command.
			else
			{
				return CommandType.C_COMMAND;
			}
		}

		/// <summary>
		/// Returns the symbol or decimal Xxx of the current command
		/// @Xxx or (Xxx).
		/// Should be called only when <see cref="Parser.CurrentCommandType()"/> is A_COMMAND or L_COMMAND.
		/// </summary>
		private string Symbol()
		{
			if (CurrentCommandType() == CommandType.A_COMMAND)
			{
				string curr = _commands.Peek();
				return curr.Substring(1);
			}
			else if (CurrentCommandType() == CommandType.L_COMMAND)
			{
				string curr = _commands.Peek();
				int len = curr.Length;
				return curr.Substring(1, len - 2);
			}
			else
				throw new Exception("Invalid command type");
		}

		/// <summary>
		/// Return the dest mnemonic in the current C-command.
		/// </summary>
		private string Dest()
		{
			if (CurrentCommandType() != CommandType.C_COMMAND)
				throw new Exception("Invalid command type");
			
			string curr = _commands.Peek();
			// http://regexr.com/3eg83
			Regex regi = new Regex("(\\w+)=");
			Match pence = regi.Match(curr);
			if (!pence.Success)
				return string.Empty;
			else
				return pence.Groups[1].Value;
		}

		/// <summary>
		/// Returns the comp mnemonic in the current C-command.
		/// </summary>
		private string Comp()
		{
			if (CurrentCommandType() != CommandType.C_COMMAND)
				throw new Exception("Invalid command type");

			string curr = _commands.Peek();
			// http://regexr.com/3efdu
			Regex regi = new Regex("(\\w+=)?([AMD10+\\-\\!\\&\\|]+)(;\\w{3})?");
			Match pence = regi.Match(curr);
			if (!pence.Success)
				return string.Empty;
			else
				return pence.Groups[2].Value;
		}

		/// <summary>
		/// Returns the jump mnemonic in the current C-command.
		/// </summary>
		private string Jump()
		{
			if (CurrentCommandType() != CommandType.C_COMMAND)
				throw new Exception("Invalid command type");

			string curr = _commands.Peek();
			// http://regexr.com/3efe1
			Regex regi = new Regex("(\\w+=)?([AMD+\\-10]+;)(\\w{3})");
			//Regex regi = new Regex("(\\w+=)?([AMD10+\\-\\!\\&\\|]+)(;\\w{3})?");
			Match pence = regi.Match(curr);
			// If it's not found, return the empty string.
			if (!pence.Success)
				return string.Empty;
			else
				return pence.Groups[3].Value;
		}
	}
}

