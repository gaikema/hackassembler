﻿using System;
using System.Collections.Generic;

namespace Assembler
{
	/// <summary>
	/// Symbol table.
	/// Keeps a correspondence between symbolic labels and numeric addresses.
	/// </summary>
	public class SymbolTable
	{
		/// <summary>
		/// The table.
		/// </summary>
		private Dictionary<string, int> _table;

		/// <summary>
		/// Initializes a new instance of the <see cref="Assembler.SymbolTable"/> class.
		/// </summary>
		public SymbolTable()
		{
			// Predefined symbols.
			_table = new Dictionary<string, int>()
			{
				{"SP", 0},
				{"LCL", 1},
				{"ARG", 2},
				{"THIS", 3},
				{"THAT", 4},
				{"SCREEN", 16384},
				{"KBD", 24576}
			};
			for (int i = 0; i <= 15; i++)
			{
				_table["R" + i.ToString()] = i;
			}
		}

		/// <summary>
		/// Adds the pair (symbol, address) to the table.
		/// </summary>
		/// <param name="symbol">Symbol.</param>
		/// <param name="address">Address.</param>
		public void AddEntry(string symbol, int address)
		{
			//_table.Add(symbol, address);
			_table[symbol] = address;
		}

		/// <summary>
		/// Contains the specified symbol.
		/// </summary>
		/// <param name="symbol">Symbol.</param>
		public bool Contains(string symbol)
		{
			return _table.ContainsKey(symbol);
		}

		/// <summary>
		/// Gets the address.
		/// </summary>
		/// <returns>The address.</returns>
		/// <param name="symbol">Symbol.</param>
		public int GetAddress(string symbol)
		{
			return _table[symbol];
		}
	}
}

