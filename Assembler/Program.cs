﻿using System;
using System.IO;

namespace Assembler
{
	/// <summary>
	/// Main class.
	/// </summary>
	class MainClass
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name="args">The command-line arguments.</param>
		public static void Main(string[] args)
		{
			//Console.WriteLine(args[0]);

			// Not enough arguments.
			if (args.Length < 1)
			{
				Console.WriteLine("Insufficient arguments.");
				return;
			}

			string file = args[0];
			Parser parse = new Parser(file);

			/*
			Lexer luthor = new Lexer(file);
			Console.WriteLine(luthor.GetLines());
			*/

			string output = parse.Parse();
			//Console.WriteLine(output);
			// Write the results of the assembler to a file with the '.out' extension.
			File.WriteAllText(Path.GetFileNameWithoutExtension(file) + ".out.hack", output);
			Console.WriteLine("File assembled.");

			/*
			* TODO: Spawn a process to assemble the script with the actual assembler and
			* compare it with diff <file>.hack <file>.out.
			* http://www.cyberciti.biz/faq/how-do-i-compare-two-files-under-linux-or-unix/
			* 
			* JK that's actually a huge pain in C#.
			* Just do all testing with Python or GUN-Make.
			*/
		}
	}
}
