﻿using System;

namespace Assembler
{
	/// <summary>
	/// Command type.
	/// </summary>
	public enum CommandType
	{
		A_COMMAND,
		C_COMMAND,
		L_COMMAND
	}
}

