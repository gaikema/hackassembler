﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;

namespace Assembler
{
	/// <summary>
	/// The lexer converts the input into commands to be handled by the <see cref="Assembler.Parser"/>.
	/// </summary>
	public class Lexer
	{
		/// <summary>
		/// The current input string.
		/// This gets modified throughout the process.
		/// </summary>
		private string lines;

		/// <summary>
		/// Initializes a new instance of the <see cref="Assembler.Lexer"/> class.
		/// </summary>
		/// <param name="fileName">File name.</param>
		public Lexer(string fileName)
		{
			// Read the entire file and store it to lines.
			lines = File.ReadAllText(fileName);
			CleanLines();
		}

		/// <summary>
		/// Removes all non-essential characters (like Yamcha).
		/// </summary>
		private void CleanLines()
		{
			RemoveComments();
			RemoveTabsAndSpaces();
			RemoveEmptyLines();
		}

		/// <summary>
		/// Removes the comments.
		/// Regex pattern for matching comments here: http://regexr.com/3eerr
		/// Here is the documentation for regex replace: https://msdn.microsoft.com/en-us/library/xwewhkd1(v=vs.110).aspx
		/// </summary>
		private void RemoveComments()
		{
			// The 'at' sign means I don't need to escape stuff.
			// http://stackoverflow.com/a/1740692/5415895
			string pattern = @"//.*|(""(?:\\[^""]|\\""|.)*?"")|(?s)/\*.*?\*/";
			lines = Regex.Replace(lines, pattern, string.Empty);
		}

		/// <summary>
		/// Removes the tabs and spaces.
		/// </summary>
		private void RemoveTabsAndSpaces()
		{
			// http://stackoverflow.com/a/10032056/5415895
			lines = Regex.Replace(lines, @"^\s+|\s+$", string.Empty, RegexOptions.Multiline);
		}

		/// <summary>
		/// Removes the empty lines.
		/// http://stackoverflow.com/a/7647762/5415895
		/// </summary>
		private void RemoveEmptyLines()
		{
			// For some reason this doesn't work on regexpr.com
			lines = Regex.Replace(lines, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline).TrimEnd();;
		}

		/// <summary>
		/// Gets the lines.
		/// </summary>
		/// <returns>The lines.</returns>
		public string GetLines()
		{
			return lines;
		}

		/// <summary>
		/// Just splits the input by line and returns that as a queue.
		/// </summary>
		/// <returns>The commands.</returns>
		public Queue<string> GetCommands()
		{
			// http://stackoverflow.com/a/1508217/5415895
			string[] result = Regex.Split(lines, "\r\n|\r|\n");
			// https://social.msdn.microsoft.com/Forums/vstudio/en-US/a19ef714-0bc4-4947-8ed2-25891e103aa9/can-i-convert-array-to-queue?forum=csharpgeneral
			return new Queue<string>(result);
		}
	}
}

