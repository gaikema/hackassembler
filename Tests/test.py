# Pass the assembly file without the extension to run.
# Example: python test.py pong/Pong

import os
import sys

if len(sys.argv) < 2 :
	raise ValueError("Insufficient arguments!")

# http://stackoverflow.com/a/3430395/5415895
file_dir = os.path.dirname(os.path.abspath(__file__))
name = sys.argv[1]
# This is the relative file path from the current directory to the nand2tetris directory.
emulator = file_dir + '/../../../nand2tetris/tools'

# Assemble
os.system('bash ' + emulator + '/Assembler.sh ' + name + '.asm')

# Test
if os.path.isfile(name + ".out.hack"):
	print("Comparing the files.")
	os.system('diff ' + name + '.out.hack ' + name + '.hack')
	print("If there was no output, the files are identical.")